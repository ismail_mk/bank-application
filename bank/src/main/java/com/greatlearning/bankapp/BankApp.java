package com.greatlearning.bankapp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

class BankOps {

	//private long balance;
	Scanner sc = new Scanner(System.in);
	Writer output = null;

	// method open the file to write log
	public void openFile() throws FileNotFoundException {

		File file = new File("F:\\transactions.txt");
		try {
			if (!file.exists()) {
				file.createNewFile();
			}

			output = new BufferedWriter(new FileWriter(file, true));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// method to write file
	public void writeFile(String msg) {

		try {
			output.write(msg);
			output.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// method to valid the account no and password
	public boolean isValidAcc(ArrayList<Cust> list, String accno, String pass) throws FileNotFoundException {

		for (Cust cust : list) {

			if ((cust.getBankAccountNo().equals(accno)) && (cust.getPassword().equals(pass))) {
				openFile();
				return true;

			}
		}
		System.out.println("Invalid Credentails");
		return false;
	}

	// method to deposit amount
	public void deposit()  {
		long amt;
		System.out.println("Enter the amount you want to deposit: ");
		amt = sc.nextLong();
		//balance = balance + amt;
		System.out.println("Amount " + amt + " deposit successfully");
		writeFile("amount deposit transaction\n");

	}

	// method to withdraw amount
	public void withdrawal()  {
		long amt;
		System.out.println("Enter the amount you want to withdraw: ");
		amt = sc.nextLong();
		// if (balance >= amt) {
		// balance = balance - amt;
		System.out.println("Amount " + amt + " withdrawal successfully");
		// } else {
		// System.out.println("Your balance is less than " + amt + "\tTransaction
		// failed...!!");
		// }
		writeFile("amount withdrawal transaction\n");
	}

	// method to transfer
	public void transfer()  {
		long otp, amt;
		String bankAccountNo;
		int randomNumber = new Random().nextInt(8999) + 1000;

		System.out.println("Enter the OTP\n");
		System.out.println(randomNumber);

		otp = sc.nextInt();

		if (randomNumber == otp) {
			System.out.println("OTP verification successful !!!\n");
			System.out.println("Enter the amount and bank account no to which you want to transfter");
			amt = sc.nextLong();
			bankAccountNo = sc.next();
			System.out.println("Amount " + amt + " transferred successfully to bank account no " + bankAccountNo);
			writeFile("amount transfer transaction\n");

		} else {
			System.out.println("OTP verification failed !!!");
			writeFile("failed transfer transaction\n");
		}
	}

}

public class BankApp {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException {

		BankOps bankOps = new BankOps();

		Cust c1 = new Cust("A001", "password");
		Cust c2 = new Cust("A002", "welcome123");
		Cust c3 = new Cust("A003", "Tiger123");

		ArrayList<Cust> customers = new ArrayList<Cust>();

		customers.add(c1);
		customers.add(c2);
		customers.add(c3);

		Scanner sc = new Scanner(System.in);

		System.out.println("Welcome to the login page");
		System.out.println("\n");

		System.out.println("Enter the Bank Account No: ");
		String accountNo = sc.next();

		System.out.println("Enter the password for the coressponding account no:");
		String password = sc.next();

		if (bankOps.isValidAcc(customers, accountNo, password)) {

			int choice;
			do {

				System.out.println("\n !!!   Welcome to Indian Banking System Application   !!!");
				System.out.println("--------------------------------------------------------------");
				System.out.println("\n");
				System.out.println("Enter the operation that you want to perform");
				System.out.println(" 1. Deposit \n 2. Withdraw \n 3. Transfer \n 0. Logout ");
				System.out.println("--------------------------------------------------------------");

				choice = sc.nextInt();

				switch (choice) {
				case 1:

					bankOps.deposit();
					break;
					
				case 2:

					bankOps.withdrawal();
					break;

				case 3:

					bankOps.transfer();
					break;

				case 0:
					System.out.println("Exited successfully");
					break;

				}
			} while (choice != 0);

		}
	}

}
